﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bollmove : MonoBehaviour {

	public float Boll_move_speed;

	void OnTriggerEnter(Collider a){
		if(a.gameObject.tag == "box"){
			Destroy(a.gameObject);
		}
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		this.gameObject.transform.position += new Vector3 (0f, 0f, -Boll_move_speed);


	}
}
